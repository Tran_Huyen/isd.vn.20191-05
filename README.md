

## The work we have done
- Design AVM and AFC Machine's use case diagram
- Analysis the flow of event of AFC machine for these following usecase:
	+ Put tickets into ticket recognizer
	+ Check ticket
	+ Put the card to Card Scanner
	+ Scan Card
	+ Charge money
	+ Control status gate
	+ Get ticket
	+ Return ticket